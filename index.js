var express = require("express");
var sql = require("mssql");

var Db = require("./db");

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get("/", async function (req, res) {
  // query to the database and get the records
  try {
    let datas = await Db.instance().query("select * from Batch");
    res.send(datas);
  } catch (e) {
    console.log(e);
  }
});

app.get("/hi", function (req, res) {
  // const { phone } = req.body;
  // console.log(phone);

  Db.instance().query("SELECT * FROM User", function (err, data) {
    if (err) {
      console.log(err);
    }
    res.send(data.recordset);
  });
});

var server = app.listen(3004, function () {
  console.log("Server is running..");
});
